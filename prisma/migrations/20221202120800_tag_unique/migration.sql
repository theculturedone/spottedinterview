/*
  Warnings:

  - A unique constraint covering the columns `[text]` on the table `Tag` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Tag_text_key" ON "Tag"("text");
