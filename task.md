# Fullstack task

Welcome to spotted.io test.

## How to submit

1.  clone this repository or download as zip file.
2.  Publish the repo without any changes to your github profile as private repository. If you use vscode there is a command `Publish to Github` you can use. If you downloaded as zip file make initial commit and push it.
3.  Make your changes, commit them, push them and share your private repo with user `capaj` on github

If you fail to follow this and submit through a zip file or by any other means, your submission will not be accepted.

### Timing

The task should take you around

- junior: 210 minutes
- medior: 140 minutes
- senior: 70 minutes
- senior with a prior experience with prisma/chakra-ui and react-query it should take you around 35 minutes

after yarn install and repo setup.

### Requirements

This is a fullstack test, so make sure you:

- modify the prisma schema
- add needed migration
- add seed data(if needed)
- implement FE changes
- modify existing tests if they are broken

### The repository starting point

We have a simple blog which at the moment has these functionalities:

- display a list of posts
- display a single post detail
- add a new post

### Task itself

Your task is to implement:

- when adding new post, you can select tags for the post from a tag input. A list of tags should be coming from backend, not hardcoded on FE
- (optional) tag input should create a new tag when the user types in any text and confirms with hitting an enter key
- display post's tags on the list of posts for each post, ideally as a list of `<Tag />` elements
- display post's tags on the post detail, ideally as a list of `<Tag />` elements

When changing the schema, please make sure that we can rename tags in the future. Tag rename is not part of this task, but the schema you introduce should allow renaming a tag without updating any Post.

Ideally your code should have at least happy paths tested. When it comes to styling feel free to keep it very basic.

Our primary focus in this task is clean code, DX and UX. The candidate who delivers the task with a codebase without any grave mistakes and best DX/UX gets hired.

You can use any libraries, any utilities that you wish, but please do not change the DB. SQlite allows us to quickly spin it up without running dockers or setting up a local DB.
