import { Box } from '@chakra-ui/react'

interface TagPropType {
  text: string
}

export const Tag: React.FC<TagPropType> = (props) => {
  return (
    <Box
      bg="blue"
      w="5%"
      p={2}
      m={1}
      textAlign="center"
      color="white"
      fontWeight="500"
      rounded={20}
    >
      {props.text}
    </Box>
  )
}
