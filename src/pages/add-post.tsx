import { trpc } from '../utils/trpc'

import { inferProcedureInput } from '@trpc/server'
import { Heading, Input, Textarea } from '@chakra-ui/react'
import { AppRouter } from '~/server/routers/_app'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { CUIAutoComplete } from 'chakra-ui-autocomplete'

const AddPost = () => {
  const [selectedItems, setSelectedItems] = useState<any>([])
  const [availableTags, setAvailableTags] = useState<any>([])

  const utils = trpc.useContext()

  const tagsQuery = trpc.tag.list.useQuery({
    limit: undefined
  })

  const addPost = trpc.post.add.useMutation({
    async onSuccess() {
      await utils.post.list.invalidate()
    }
  })

  const addTag = trpc.tag.add.useMutation({
    async onSuccess() {
      await utils.tag.list.invalidate()
    }
  })

  const handleCreateItem = async (item) => {
    type TagAddInput = inferProcedureInput<AppRouter['tag']['add']>
    const input: TagAddInput = {
      text: item.value
    }
    try {
      const newTag = await addTag.mutateAsync(input)
      setSelectedItems((oldItems) => {
        return [
          ...oldItems,
          {
            label: newTag.text,
            value: newTag
          }
        ]
      })
    } catch (cause) {
      console.error({ cause }, 'Failed to add new tag')
    }
  }

  const handleSelectedItemsChange = (selectedItems) => {
    if (selectedItems) {
      setSelectedItems(selectedItems)
    }
  }

  const router = useRouter()

  useEffect(() => {
    setAvailableTags(tagsQuery.data?.items.map((tag) => {
      return {
        label: tag.text,
        value: tag
      }
    }))
  }, [tagsQuery.data?.items])

  return (
    <>
      <Heading size="lg">Add a Post</Heading>

      <form
        onSubmit={async (e) => {
          e.preventDefault()
          const $form = e.currentTarget
          const values = Object.fromEntries(new FormData($form))
          type Input = inferProcedureInput<AppRouter['post']['add']>
          //    ^? -> is the type that we get by using trpc
          const input: Input = {
            title: values.title as string,
            text: values.text as string,
            tags: selectedItems.length
              ? selectedItems.map((item) => item.value.id)
              : []
          }
          try {
            await addPost.mutateAsync(input)

            $form.reset()
            router.push('/')
          } catch (cause) {
            console.error({ cause }, 'Failed to add post')
          }
        }}
      >
        <label htmlFor="title">Title:</label>
        <br />
        <Input
          id="title"
          name="title"
          type="text"
          disabled={addPost.isLoading}
        />

        <br />
        <label htmlFor="text">Text:</label>
        <br />
        <Textarea id="text" name="text" disabled={addPost.isLoading} />
        <br />
        <CUIAutoComplete
          label="Choose tags for this post"
          key="tags"
          placeholder="Search for tags"
          onCreateItem={handleCreateItem}
          items={availableTags}
          tagStyleProps={{
            rounded: 'full',
            pt: 1,
            pb: 2,
            px: 2,
            fontSize: '1rem'
          }}
          selectedItems={selectedItems}
          onSelectedItemsChange={(changes) =>
            handleSelectedItemsChange(changes.selectedItems)
          }
        />
        <Input type="submit" disabled={addPost.isLoading} />
        {addPost.error && (
          <p style={{ color: 'red' }}>{addPost.error.message}</p>
        )}
      </form>
    </>
  )
}

export default AddPost
