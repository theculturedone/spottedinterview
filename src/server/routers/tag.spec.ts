/**
 * Integration test for the `tag` router
 */
import { createContextInner } from '../context'
import { appRouter } from './_app'
import { describe, expect, it } from 'vitest'
import { faker } from '@faker-js/faker'

describe('tag', () => {
  it('add and get tag', async () => {
    const ctx = await createContextInner({})
    const caller = appRouter.createCaller(ctx)
    faker.seed()
    const randomText = faker.random.word()
    const tag = await caller.tag.add({
      text: randomText
    })
    const byId = await caller.tag.byId({ id: tag.id })

    expect(byId.text).toMatchInlineSnapshot('"' + randomText + '"')
  })
})
